package com.tgl.exercise100;

public class RemoveVowels {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		star(s);
	}
	
	public static void star(String str) {
		str = str.replaceAll("[aeiouAEIOU]", "*");
		System.out.println(str);
	}

}
