package com.tgl.exercise100;

public class ReverseAnArray {

	public static void main(String[] args) {
		String[] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		reverse(breakfast);
	}
	
	public static void reverse(String[] array) {
		for (int i = array.length-1; i >= 0; i--) {
			System.out.println(array[i]);
		}
	}
	
}
