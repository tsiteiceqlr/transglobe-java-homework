package com.tgl.exercise100;

public class PowerOf2 {

	public static void main(String[] args) {
		powers(8);
	}
	
	public static void powers(int n) {
		int sum = 1;
		for (int i = 0; i < n; i++) {
			sum *= 2;
			System.out.println(sum);
		}
	}

}
