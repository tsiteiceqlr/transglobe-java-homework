package com.tgl.exercise100;

public class OddNumbers {

	public static void main(String[] args) {
		OddNumbers(20);
	}
	
	public static void OddNumbers(int number) {
		for (int i = 0; i <= number; i++) {
			if (i % 2 == 1) {
				System.out.println(i);
			}
		}
	}
	
}
