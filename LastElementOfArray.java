package com.tgl.exercise100;

public class LastElementOfArray {

	public static void main(String[] args) {
		String[] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		lastElement(breakfast);
	}
	
	public static void lastElement(String[] array) {
		System.out.println(array[array.length-1]);
	}

}
