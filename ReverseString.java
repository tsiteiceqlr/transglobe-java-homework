package com.tgl.exercise100;

public class ReverseString {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		reverse(s);
	}
	
	public static void reverse(String str) {
		for (int i = str.length()-1; i >= 0; i--) {
			System.out.print(str.charAt(i));
		}
	}

}
