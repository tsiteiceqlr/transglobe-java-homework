package com.tgl.exercise100;

public class PalindromicArrays {

	public static void main(String[] args) {
		String[] palindromic = {"Sausage", "Eggs", "Beans", "Beans", "Eggs", "Sausage"};
		String[] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		
		System.out.println(isPalindrome(palindromic));
		System.out.println(isPalindrome(breakfast));		
	}
	
	public static Boolean isPalindrome(String[] array) {
		for (int i = 0; i < array.length/2; i++) {
			if (array[i] != array[array.length-1-i]) {
				return false;
			}
		}		
		return true;
	}

}
