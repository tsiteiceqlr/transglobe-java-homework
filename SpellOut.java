package com.tgl.exercise100;

public class SpellOut {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		spellOut(s);
	}
	
	public static void spellOut(String str) {
		str = str.toUpperCase();
		String[] strA = str.split(" ");
		for (int i = 0; i < strA.length; i++) {
			StringBuilder sb = new StringBuilder(strA[i]);
			for (int j = 0; j < (strA[i].length()-1)*2; j += 2) {
				sb.insert(j+1, "-");
			}
			System.out.print(" " + sb);
		}
	}

}
