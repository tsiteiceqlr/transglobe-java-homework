package com.tgl.exercise100;

public class Palindromes2 {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		String p = "Rise to vote, Sir!";
		
		System.out.println(isPalindrome(s));
		System.out.println(isPalindrome(p));
	}
	
	public static boolean isPalindrome(String str) {
		str = str.replaceAll("[^A-Za-z0-9]", "");
		str = str.toLowerCase();
		for (int i = 0; i < str.length() / 2; i++) {
			if (str.charAt(i) != str.charAt(str.length()-1-i)) {
				return false;
			}
		}
		return true;
	}
}
