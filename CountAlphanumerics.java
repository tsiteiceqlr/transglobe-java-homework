package com.tgl.exercise100;

public class CountAlphanumerics {

	public static void main(String[] args) {
		String s = "1984 by George Orwell.";
		countChars(s);
	}
	
	public static void countChars(String str) {
		int count = 0;
		str = str.replaceAll("[^A-Za-z0-9]", "");
		for (int i = 0; i < str.length(); i++) {
			count++;
		}
		System.out.println(count);
	}

}
