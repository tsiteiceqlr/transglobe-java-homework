package com.tgl.exercise100;

import java.util.*;

public class SubstitutionCipher {

	public static void main(String[] args) {
		String s = "Hello World";
		encode(s);
	}
	
	public static void encode(String str) {
		str = str.toLowerCase();
		char myChar;
		int char2AsciiCode;
		String[] strA = str.split(" ");
		for (int i = 0; i < strA.length; i++) {
			List<Integer> asciiList = new ArrayList<Integer>();
			for (int j = 0; j < strA[i].length(); j++) {
				myChar = strA[i].charAt(j);
				char2AsciiCode = (int)myChar - 96;
				asciiList.add(char2AsciiCode);
				//System.out.print(char2AsciiCode + ",");
			}
			System.out.print(asciiList);
		}
	}

}
