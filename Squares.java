package com.tgl.exercise100;

public class Squares {

	public static void main(String[] args) {
		squares(100);
	}
	
	public static void squares(int number) {
		for (int i = 1; i <= number; i++) {
			for (float j = 1; j <= i; j++) {
				if (i / j == j) {
					System.out.println(i);
				}
			}
		}
	}

}
