package com.tgl.exercise100;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int year = scanner.nextInt();
		leap(year);
	}
	
	public static void leap(int year) {
		if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
			System.out.printf("%d is a leap year", year);
		} else {
			System.out.printf("%d is not a leap year", year);
		}
	}

}
