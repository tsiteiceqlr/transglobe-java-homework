package com.tgl.exercise100;

public class CountTheLetterE {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		countEs(s);
	}
	
	public static void countEs(String str) {
		char someChar = 'e';
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == someChar) {
				count++;
			}
		}
		System.out.println(count);
	}

}
