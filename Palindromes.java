package com.tgl.exercise100;

public class Palindromes {

	public static void main(String[] args) {
		String sentence = "I never saw a purple cow";
		String palindrome = "rotavator";

		System.out.println(isPalindrome(sentence));
		System.out.println(isPalindrome(palindrome));
	}
	
	public static boolean isPalindrome(String str) {
		for (int i = 0; i < str.length() / 2; i++) {
			if (str.charAt(i) != str.charAt(str.length()-1-i)) {
				return false;
			}
		}
		return true;
	}

}
