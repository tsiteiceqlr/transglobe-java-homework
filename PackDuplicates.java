package com.tgl.exercise100;

import java.util.*;

public class PackDuplicates {

	public static void main(String[] args) {
		char[] letters = {'a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e'};
		pack(letters);
	}
	
	public static void pack(char[] array) {
		char temp = array[0];
		List<String> strA = new ArrayList<String>();
		String sb = "";
		for (int i = 0; i < array.length; i++) {
			if (array[i] == temp) {
				sb += Character.toString(array[i]);
				temp = array[i];
			} else {
				strA.add(sb);
				temp = array[i];
				sb = "";
				sb += Character.toString(array[i]);
			}
		}
		strA.add(sb);
		System.out.println(strA);
	}

}
