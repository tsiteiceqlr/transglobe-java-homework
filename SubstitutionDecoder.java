package com.tgl.exercise100;

public class SubstitutionDecoder {

	public static void main(String[] args) {
		String s = "9 14,5,22,5,18 19,1,23 1 16,21,18,16,12,5 3,15,23";
		decode(s);
	}
	
	public static void decode(String str) {
		String[] strA;
		String[] strA1;
		strA = str.split(" ");
		for (int i = 0; i < strA.length; i++) {
			strA1 = strA[i].split(",");
			int intValue;
			char asciiCode2Char;
			for (int j = 0; j < strA1.length; j++) {
				intValue = Integer.parseInt(strA1[j]) + 96;
				asciiCode2Char = (char)intValue;
				System.out.print(asciiCode2Char);
			}
			System.out.print(" ");
			
		}
	}

}
